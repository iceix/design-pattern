package chapter01.iterator;

import chapter01.iterator.Book;
import chapter01.iterator.BookShelf;
import chapter01.iterator.Iterator;
import org.junit.jupiter.api.Test;

class IteratorTest {

    @Test
    public void test() {
        BookShelf bookShelf = new BookShelf();
        Iterator<Book> iterator = bookShelf.iterator();
        Book book1 = new Book("三国演义");
        Book book2 = new Book("红楼梦");
        Book book3 = new Book("水浒传");
        Book book4 = new Book("西游记");
        bookShelf.append(book1);
        bookShelf.append(book2);
        bookShelf.append(book3);
        bookShelf.append(book4);

        while (iterator.hasNext()) {
            Book book = iterator.next();
            System.out.println(book.getName());
        }
    }

}