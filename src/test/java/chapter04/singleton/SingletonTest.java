package chapter04.singleton;

import chapter05.singleton.Triple;
import org.junit.jupiter.api.Test;

class SingletonTest {
    @Test
    public void Test(){
        Triple instance00 = Triple.getInstance(0);
        Triple instance01 = Triple.getInstance(0);
        Triple instance10 = Triple.getInstance(1);
        Triple instance11 = Triple.getInstance(1);
        Triple instance20 = Triple.getInstance(2);
        Triple instance21 = Triple.getInstance(2);

        if (instance00 == instance01) {
            System.out.println("0号单例成功");
        }

        if (instance10 == instance11) {
            System.out.println("1号单例成功");
        }

        if (instance20 == instance21) {
            System.out.println("2号单例成功");
        }

        if (instance00 != instance10 && instance10 != instance20) {
            System.out.println("0、1、2号单例成功");
        }
    }
}