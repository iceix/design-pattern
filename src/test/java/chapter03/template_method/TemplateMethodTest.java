package chapter03.template_method;

import chapter03.template_method.AbstractDisplay;
import chapter03.template_method.CharDisplay;
import chapter03.template_method.StringDisplay;
import org.junit.jupiter.api.Test;

class TemplateMethodTest {
    @Test
    public void test() {
        AbstractDisplay abstractDisplay1 = new CharDisplay('H');
        AbstractDisplay abstractDisplay2 = new StringDisplay("Hello, world.");

        abstractDisplay1.display();
        abstractDisplay2.display();
    }
}