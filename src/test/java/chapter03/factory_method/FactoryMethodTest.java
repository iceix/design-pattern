package chapter03.factory_method;

import chapter04.factory_method.framwork.Product;
import chapter04.factory_method.idcard.IdCardFactory;
import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.Set;

public class FactoryMethodTest {

    @Test
    public void test(){
        IdCardFactory factory = new IdCardFactory();
        Product card1 = factory.create("Mary");
        Product card2 = factory.create("Jack");
        Product card3 = factory.create("Rose");
        card1.use();
        card2.use();
        card3.use();

        System.out.println("工厂中所有的卡：");
        Set<Map.Entry<String, String>> entries = factory.getCards().entrySet();
        for (Map.Entry<String, String> entry : entries) {
            System.out.println(entry);
        }
    }
}
