package chapter06.prototype;

import chapter06.prototype.framework.Manager;
import chapter06.prototype.framework.Product;
import org.junit.jupiter.api.Test;

class PrototypeTest {
    @Test
    public void test(){
        Manager manager = new Manager();
        UnderlinePen underlinePen = new UnderlinePen('~');
        MessageBox messageBox = new MessageBox('*');

        manager.register("strong message", underlinePen);
        manager.register("warning box", messageBox);

        Product p1 = manager.create("strong message");
        Product p2 = manager.create("warning box");
        p1.use("Hello, world.");
        p2.use("Hello, world.");
    }
}