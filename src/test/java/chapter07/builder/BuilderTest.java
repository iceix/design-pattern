package chapter07.builder;

import org.junit.jupiter.api.Test;

class BuilderTest {
    @Test
    public void test0(){
        TextBuilder textBuilder = new TextBuilder();
        Director director = new Director(textBuilder);
        director.construct();
        System.out.println(textBuilder.getResult());
    }

    @Test
    public void test1(){
        HtmlBuilder htmlBuilder = new HtmlBuilder();
        Director director = new Director(htmlBuilder);
        director.construct();
        System.out.println(htmlBuilder.getResult());
    }

}