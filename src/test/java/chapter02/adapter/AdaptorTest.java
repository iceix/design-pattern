package chapter02.adapter;

import chapter02.adapter.FileIo;
import chapter02.adapter.FileProperties;
import org.junit.jupiter.api.Test;

class AdaptorTest {
    public static final String READ_FILE_NAME = "file.txt";
    public static final String WRITE_FILE_NAME = "new_file.txt";

    @Test
    public void Test() {
        FileIo fileIo = new FileProperties();
        try {
            fileIo.readFromFile(READ_FILE_NAME);
            fileIo.setValue("year", "2023");
            fileIo.setValue("month", "3");
            fileIo.setValue("day", "9");
            fileIo.writeToFile(WRITE_FILE_NAME);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}