package chapter05.singleton;

public class Triple {
    int id;
    private static volatile Triple[] triples = new Triple[3];

    private Triple(int id) {
        id = id % 3;
        this.id = id;
    }

    public static Triple getInstance(int id) {
        id = id % 3;
        if (triples[id] == null) {
            synchronized (Triple.class) {
                if (triples[id] == null) {
                    triples[id] = new Triple(id);
                }
            }
        }
        return triples[id];
    }
}
