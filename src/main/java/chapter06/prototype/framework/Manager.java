package chapter06.prototype.framework;

import java.util.HashMap;
import java.util.Map;

/**
 * @author iceixQwQ
 * @create 2023-03-10
 */
public class Manager {
    private Map<String, Product> showcases = new HashMap<>();

    public void register(String name, Product product) {
        showcases.put(name, product);
    }

    public Product create(String name) {
        Product product = showcases.get(name);
        return product == null ? null : product.createClone();
    }
}
