package chapter06.prototype.framework;

/**
 * @author iceixQwQ
 * @create 2023-03-10
 */
public abstract class Product implements Cloneable {
    public abstract void use(String s);

    public Product createClone() {
        Product clone = null;
        try {
            clone = (Product) this.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return clone;
    }
}
