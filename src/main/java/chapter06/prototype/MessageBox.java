package chapter06.prototype;

import chapter06.prototype.framework.Product;
import lombok.AllArgsConstructor;

/**
 * @author iceixQwQ
 * @create 2023-03-10
 */
@AllArgsConstructor
public class MessageBox extends Product {
    private char decorativeChar;

    @Override
    public void use(String s) {
        int length = s.getBytes().length;
        for (int i = 0; i < length + 2; i++) {
            System.out.print(decorativeChar);
        }
        System.out.println();
        System.out.println(decorativeChar + s + decorativeChar);
        for (int i = 0; i < length + 2; i++) {
            System.out.print(decorativeChar);
        }
        System.out.println();
    }
}
