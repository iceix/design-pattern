package chapter06.prototype;

import chapter06.prototype.framework.Product;
import lombok.AllArgsConstructor;

/**
 * @author iceixQwQ
 * @create 2023-03-10
 */
@AllArgsConstructor
public class UnderlinePen extends Product {
    private char underlineChar;

    @Override
    public void use(String s) {
        int length = s.getBytes().length;
        System.out.println('\\' + s + '\\');
        System.out.print(" ");
        for (int i = 0; i < length; i++) {
            System.out.print(underlineChar);
        }
        System.out.println();
    }
}
