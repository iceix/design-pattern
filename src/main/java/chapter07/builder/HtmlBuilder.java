package chapter07.builder;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class HtmlBuilder implements Builder {
    private String fileName;
    private PrintWriter printWriter;

    @Override
    public void makeTitle(String title) {
        fileName = title + ".html";
        try {
            printWriter = new PrintWriter(new FileWriter(fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
        printWriter.println("" +
                "<html>\n" +
                "   <head>\n" +
                "       <title>\n" +
                "           " + title + "\n" +
                "       </title>\n" +
                "   </head>\n" +
                "   <body>\n" + "" +
                "       <h1>\n" +
                "           " + title + "\n" +
                "       </h1>");
    }

    @Override
    public void makeString(String str) {
        printWriter.println("" +
                "       <p>\n" +
                "           " + str + "\n" +
                "       </p>");
    }

    @Override
    public void makeItems(String[] items) {
        printWriter.println("" +
                "       <ul>");
        for (int i = 0; i < items.length; i++) {
            printWriter.println("" +
                    "           <li>\n" +
                    "               " + items[i] + "\n" + "" +
                    "          </li>");
        }
        printWriter.println("" +
                "       </ul>");
    }

    @Override
    public void close() {
        printWriter.println("" +
                "   </body>\n" +
                "</html>");
        printWriter.close();
    }

    public String getResult() {
        return fileName;
    }
}
