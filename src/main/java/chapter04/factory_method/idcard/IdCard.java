package chapter04.factory_method.idcard;

import chapter04.factory_method.framwork.Product;


public class IdCard extends Product {
    private String id;
    private String owner;

    IdCard() {
    }

    protected void setId(String id) {
        this.id = id;
    }

    protected void setOwner(String owner) {
        this.owner = owner;
    }

    public String getId() {
        return id;
    }

    public String getOwner() {
        return owner;
    }

    @Override
    public void use() {
        System.out.println("使用" + owner + "的ID卡");
    }
}
