package chapter04.factory_method.idcard;

import chapter04.factory_method.framwork.Factory;
import chapter04.factory_method.framwork.Product;

import java.util.HashMap;
import java.util.Map;

public class IdCardFactory extends Factory {

    Map<String, String> cards = new HashMap<>();

    @Override
    public Product create(String owner) {
        Product product = createProduct();
        registerProduct(product, owner);
        return product;
    }

    @Override
    protected Product createProduct() {
        IdCard idCard = new IdCard();
        idCard.setId(String.valueOf(System.currentTimeMillis()));
        System.out.println("工厂制作了一张id为" + idCard.getId() + "的ID卡");
        return idCard;
    }

    @Override
    protected void registerProduct(Product product, String owner) {
        System.out.println("工厂为" + owner + "注册了id为" + ((IdCard) product).getId() + "的ID卡");
        cards.put(owner, ((IdCard) product).getId());
        ((IdCard) product).setOwner(owner);
    }

    public Map<String, String> getCards() {
        return cards;
    }
}
