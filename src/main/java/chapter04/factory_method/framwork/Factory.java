package chapter04.factory_method.framwork;


public abstract class Factory {
    public abstract Product create(String owner);

    protected abstract Product createProduct();

    protected abstract void registerProduct(Product product, String Owner);
}
