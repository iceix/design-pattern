package chapter04.factory_method.framwork;

public abstract class Product {
    public abstract void use();
}
