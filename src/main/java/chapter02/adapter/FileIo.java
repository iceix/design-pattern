package chapter02.adapter;

import java.io.IOException;

/**
 * Role: Target
 *
 * @author iceixQwQ
 * @create 2023-03-09
 */
public interface FileIo {
    void readFromFile(String filename) throws IOException;

    void writeToFile(String filename) throws IOException;

    void setValue(String key, String value);

    String getValue(String key);

}
