package chapter02.adapter;

import java.io.*;
import java.nio.file.*;
import java.util.Properties;

/**
 * Role: Adapter
 * @author iceixQwQ
 * @create 2023-03-09
 */
public class FileProperties extends Properties implements FileIo {

    @Override
    public void readFromFile(String filename) throws IOException {
        Path path = Paths.get(filename);
        File file = new File(filename);
        if (!file.exists()){
            file.createNewFile();
        }
        InputStream inputStream = Files.newInputStream(path);
        load(inputStream);
        inputStream.close();
    }

    @Override
    public void writeToFile(String filename) throws IOException {
        OutputStream outputStream = Files.newOutputStream(Paths.get(filename));
        this.store(outputStream, "written by FileProperties");
        outputStream.close();
    }

    @Override
    public void setValue(String key, String value) {
        setProperty(key, value);
    }

    @Override
    public String getValue(String key) {
        return getProperty(key);
    }
}
