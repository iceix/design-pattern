package chapter01.iterator;

import java.util.ArrayList;
import java.util.List;

public class BookShelf implements Aggregate<Book>{
    private List<Book> books;
    private int last = 0;

    public BookShelf() {
        books = new ArrayList<>();
    }

    public void append(Book book) {
        books.add(book);
        last++;
    }

    public Book getBookAt(int index) {
        return books.get(index);
    }

    public int getLength() {
        return last;
    }

    @Override
    public Iterator<Book> iterator() {
        return new BookShelfIterator(this);
    }
}
