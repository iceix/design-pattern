package chapter01.iterator;

public interface Aggregate<E> {
    Iterator<E> iterator();
}
