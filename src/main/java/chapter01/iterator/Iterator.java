package chapter01.iterator;

public interface Iterator<E> {
    boolean hasNext();

    E next();
}
